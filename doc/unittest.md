<!--  -*- Mode: GFM; -*-                                                       -->
<!--                                                                           -->
<!--  © Copyright Jamie A. Jennings 2019                                       -->
<!--  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  -->
<!--  AUTHOR: Jamie A. Jennings                                                -->

# RPL 1.2 Unit Test Reference

## Running unit tests

The `rosie test` command runs any unit tests found in an RPL file:

    rosie test <filename>+

For example:

	$ rosie test rpl/net.rpl rpl/num.rpl
	rpl/net.rpl
		All 147 tests passed
	rpl/num.rpl
		All 98 tests passed
	$ 

Unless all tests pass, as shown above, the `test` command will print a list of
issues and a count of each type:

	$ rosie test test/num-errors.rpl
	test/num-errors.rpl
		FAILED: float did not accept ABC
		FAILED: float did not include exp with input "10"
		FAILED: float did not include exp with input "20"
		BLOCKED: cannot test if float includes/excludes exp because float did not accept "Aardvark"
		ERROR: pattern did not compile (or is not defined): hex
		ERROR: pattern did not compile (or is not defined): hex
		Tests: 68  Errors: 2  Failures: 3  Blocked: 1  Passed: 62
	$


## Explanation of test results

| Status  | Meaning |
| --------|-------- |
| FAILED  | An acceptance test failed to match the input; or                         |
|         | a rejection test did match the input; or                                 |
| 		  | an inclusion test did not include the specified type; or                 |
| 		  | an exclusion test did include the specified type.                        |
| BLOCKED | An inclusion/exclusion test can only run if the input string is accepted |
| ERROR   | The pattern being tested is not defined (or did not compile)             |


## Writing unit tests

Unit tests are written in comments in RPL files, on lines that start with `--
test`. They are a useful way to document what kinds of input a pattern should
accept, and what it should reject.

Tests may appear anywhere in an RPL file.  They do not need to appear after the
patterns that they test.


### Accept/Reject

The most basic test is whether a pattern will match a set of input strings.
These are called _acceptance_ tests and use the keyword `accepts`.  Also
important is to ensure that a pattern appropriately `rejects` certain inputs.
These are _rejection_ tests.

    -- test <pattern> accepts <string> [, <string>]*
    -- test <pattern> rejects <string> [, <string>]*

For example:

    -- test float accepts "-1.32", "6.02E23", "+0.314e1", "123", "-1", "+0"
    -- test float rejects "0x01", "--2", "a", "3e", "3.14e", "1."


### Includes/Excludes

When a pattern matches an input string, we sometimes want to test _how_ it
matches.  Since patterns are typically composed out of other patterns, Rosie
supports a simple test: When the tested pattern matches an input string, does
the match include an instance of another ("sub-") pattern?

An _inclusion_ test passes when the named sub-pattern is included, and an
_exclusion_ test passes when the named sub-pattern is *not* included.

    -- test <pattern> includes <pattern> <string> [, <string>]*
    -- test <pattern> excludes <pattern> <string> [, <string>]*

For example:

	-- test float includes mantissa "6.02e23"
	-- test float includes exp "6.02e23"
	-- test float excludes exp "3.1415"


### Testing local patterns

When definitions are declared `local` in RPL, they are visible only within the
file where they appear.  Local definitions are tested with `-- test local`.


For example:

	-- test local mantissa accepts "1.23", "+1.23", "-1.23", "12"
	-- test local mantissa rejects "bob", "1."

