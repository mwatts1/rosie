-- -*- Mode: Lua; -*-                                                               
--
-- net-ipv4-test.lua
--
-- © Copyright Jamie A. Jennings 2019.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHOR: Jamie A. Jennings

-- test/run test/net-ipv4-test.lua

assert(TEST_HOME, "TEST_HOME is not set")

list = import "list"
cons, map, flatten, member = list.cons, list.map, list.flatten, list.member
common = import "common"
violation = import "violation"

check = test.check
heading = test.heading
subheading = test.subheading

test.start()

heading("net.ipv4 pattern testing")
subheading("acceptance tests")

e = rosie.engine.new()
check(e)
check((e:import('net')))
netipv4 = e:compile('net.ipv4')

function check_some_acceptances()
   for i = 0, 255 do
      local teststring = tostring(i) .. ".1.1.1"
      local match, leftover = netipv4:match(teststring)
      assert(match and (leftover == 0))
      teststring = "1." .. tostring(i) .. ".1.1"
      match, leftover = netipv4:match(teststring)
      assert(match and (leftover == 0))
      teststring = "1.1." .. tostring(i) .. ".1"
      match, leftover = netipv4:match(teststring)
      assert(match and (leftover == 0))
      teststring = "1.1.1." .. tostring(i)
      match, leftover = netipv4:match(teststring)
      assert(match and (leftover == 0))
   end
   return true
end

check(check_some_acceptances())

subheading("rejection tests")

function check_some_rejections()
   for i = 256, 1300 do
      local teststring = tostring(i) .. ".1.1.1"
      local match, leftover = netipv4:match(teststring)
      assert(not match)
      teststring = "1." .. tostring(i) .. ".1.1"
      match, leftover = netipv4:match(teststring)
      assert(not match)
      teststring = "1.1." .. tostring(i) .. ".1"
      match, leftover = netipv4:match(teststring)
      assert(not match)
      teststring = "1.1.1." .. tostring(i)
      match, leftover = netipv4:match(teststring)
      assert(not match or leftover > 0)		    -- 1.1.1.256 matches with 1 leftover
   end
   return true
end

check(check_some_rejections())

return test.finish()
