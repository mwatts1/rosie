TO GENERATE RPLX (compiled rpl) FILES:

OVERVIEW
--------

1. Use the rosie "lua debug" capability, which allows the user to
access a lua repl with the rosie code loaded.

2. Create an "rplx object" by compiling an expression.  This object
contains references to the engine that created it, and to a pattern
structure.

3. Save the "peg" field of the pattern to a file.  By convention,
these files have the ".rplx" extension.

4. Use the saved rplx file at the command line using sample programs
like 'match' (matching) and 'dis' (disassemble).


DETAILS
-------

1. Compile rosie with LUADEBUG=1, e.g.

    ~/Projects/dev$ git checkout dev
    Already on 'dev'
    Your branch is up to date with 'origin/dev'.
    ~/Projects/dev$ make LUADEBUG=1
    C compiler cc appears to be working
    bsd/stdio.h appears to be installed (or is not needed)
    libreadline and readline.h appear to be installed

    [snip]

    Rosie Pattern Engine 1.2.1 built successfully!
	Use 'make install' to install into DESTDIR=/usr/local
	Use 'make uninstall' to uninstall from DESTDIR=/usr/local
	To run rosie from the build directory, use ./bin/rosie
	Try this example, and look for color text output: rosie match all.things test/resolv.conf

   Now you can run rosie with the "-D" (interactive debugging) switch:
   
    ~/Projects/dev$ bin/rosie -D
    Usage: rosie command [options] pattern file [...]
    Use the 'help' command to see all commands.
    Entering Lua 5.3.2  Copyright (C) 1994-2015 Lua.org, PUC-Rio
    >

2. Create a rosie engine in the lua environment, and then import
libraries, load files, or load strings of rpl as needed.

    > e = rosie.engine.new()
    > e
    <engine: 0x7fdda7763480>
    > e:import('net')
    true	net	table: 0x7fdda7605c70
    > e:loadfile('src/rpeg/test/data/syslog.rpl')
    true	nil	table: 0x7fdda768cf40
    > syslog = e:compile('syslog')
    > syslog
    <rplx: 0x7fdda91c3290>
    > all_ipv4 = e:compile('findall:net.ipv4')
    > all_ipv4
    <rplx: 0x7fddab924eb0>
    >

3. Save compiled patterns to files using the internal 'saveRPLX'
function.  E.g.

    > rosie.env.lpeg.saveRPLX(syslog.pattern.peg, "/tmp/syslog.rplx")
    > rosie.env.lpeg.saveRPLX(all_ipv4.pattern.peg, "/tmp/findall:net.ipv4.rplx")
    >

4. Examine the saved files, if you wish, using dis.c (a prototype, or
"hack") disassembler.  First, compile it.

    ~/Projects/dev$ cd src/rpeg/util/
    ~/Projects/dev/src/rpeg/util$ make
    cc -fvisibility=hidden -Wall -Wextra -pedantic -Waggregate-return -Wcast-align -Wcast-qual -Wdisabled-optimization -Wpointer-arith -Wshadow -Wsign-compare -Wundef -Wwrite-strings -Wbad-function-cast -Wmissing-prototypes -Wnested-externs -Wstrict-prototypes -Wunreachable-code -Wno-missing-declarations  -O2  -DNDEBUG  -std=c99 -I/Users/jjennings/Projects/dev/src/rpeg/include -fPIC   -c -o dis.o dis.c
    cc -o dis dis.o ../runtime/*.o
    ~/Projects/dev/src/rpeg/util$ ./dis /tmp/syslog.rplx 
    File: /tmp/syslog.rplx

    Symbol table:
       1: date.day
       2: date.month
       3: date.rfc3339
       4: date.year
       5: datetime
       6: message
       7: net.ipv4
       8: num.int
       9: syslog
      10: time.hour
      11: time.minute
      12: time.numoffset
      13: time.offset
      14: time.rfc3339
      15: time.rfc3339_time
      16: time.secfrac
      17: time.second
      18: word.any
      19: ~

    Code:
       0  opencapture RosieCap #9
       2  opencapture RosieCap #5
       4  opencapture RosieCap #3
       6  opencapture RosieCap #4
       8  set [(30-39)]
      17  set [(30-39)]
    [snip]
    1251  commit JMP to 1254
    1253  any 
    1254  partialcommit JMP to 1109
    1256  closecapture 
    1257  closecapture 
    1258  end 

    Codesize: 1259 instructions, 5036 bytes
    Symbols: 19 symbols in a block of 181 bytes; 19 unique symbols, and 0 are dups of 0 distinct symbols

    ~/Projects/dev/src/rpeg/util$ 


   Use the rplx file to match against input data using the (prototype,
   "hack") match.c program in the rpeg/test directory.  First, compile
   'match'.  There may be warnings.


    ~/Projects/dev/src/rpeg/util$ cd ../test/
    ~/Projects/dev/src/rpeg/test$ make
    cc -fvisibility=hidden -Wall -Wextra -pedantic -Waggregate-return -Wcast-align -Wcast-qual -Wdisabled-optimization -Wpointer-arith -Wshadow -Wsign-compare -Wundef -Wwrite-strings -Wbad-function-cast -Wmissing-prototypes -Wnested-externs -Wstrict-prototypes -Wunreachable-code -Wno-missing-declarations  -O2  -DNDEBUG  -std=c99 -I/Users/jjennings/Projects/dev/src/rpeg/include -fPIC   -c -o match.o match.c
    match.c:31:6: warning: no previous prototype for function 'error' [-Wmissing-prototypes]
    void error(const char *message, const char *additional) {
	 ^
    1 warning generated.
    cc -o match match.o -I../include ../runtime/*.o
    ~/Projects/dev/src/rpeg/test$ ./match /tmp/syslog.rplx data/log10.txt 
    {"type":"syslog","s":1,"subs":[{"type":"datetime","s":1,"subs":[{"type":"date  [snip] 
    ~/Projects/dev/src/rpeg/test$ 

   Some rplx files have been generated already, and are in the
   src/rpeg/test/data directory.

